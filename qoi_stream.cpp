#include <cstdio>
#include <cstdlib>

#define __STDC_CONSTANT_MACROS

#include <fstream>
#include <iostream>
#include <avcodec/avcodec.h>
#include <avcodec/packet.h>
#include <avformat/avformat.h>
#include <avdevice/avdevice.h>
#include <avutil/dict.h>
#include <stdexcept>

#include <opencv2/opencv.hpp>
#define QOI_IMPLEMENTATION
#define QOI_MALLOC(sz) reinterpret_cast<unsigned char*>(malloc(sz))
#define QOI_FREE(sz) free(sz)
#include "qoi/qoi.h"

using namespace std;
using namespace cv;
using namespace avpp;

uint32_t const width = 1280, height = 720, framerate = 10;
typedef cv::Point3_<uint8_t> Pixel;

AVFormatContextPtr OpenCam(std::string const& device_url) {
    AVDictionaryPtr options = dict_parse_string("framerate=" + std::to_string(framerate)
            + ",video_size=" + std::to_string(width) + "x" + std::to_string(height)
            + ",pixel_format=yuyv422");
    // check video source
    return open_input(device_url, "v4l2", options.get());
} // end function

AVPacketPtr CaptureCam(AVFormatContext &pAVFormatContext) {
    auto packet = packet_alloc();
    auto const ret = av_read_frame(&pAVFormatContext, packet.get());
    if (ret || packet->size == 0) {
        cout << "Failed to capture image" << endl;
    }
    return std::move(packet);
} // end function

size_t encode(cv::Mat const &image, ofstream& out) {
    int len = 0;
    qoi_desc const desc{static_cast<unsigned int>(image.cols), static_cast<unsigned int>(image.rows), 3, QOI_SRGB};
    void* data = qoi_encode(static_cast<void*>(image.data), &desc, &len);
    if (data == nullptr || len == 0) return 0;
    out.write(reinterpret_cast<char*>(&len), sizeof(len) / sizeof(char));
    out.write(static_cast<char*>(data), len);
    free(data);
    return 1;
}

uint8_t static inline encode_int(int8_t const& a) {
    return (a >= 0) ? (a << 1) : (((-(a + (int8_t)1)) << 1) + 1);
}

uint8_t static inline encoded_diff(uint8_t const& a, uint8_t const& b) {
    uint8_t unsigned_diff;
    if (a >= b) {
        unsigned_diff = a - b;
        if (unsigned_diff <= 127) return encode_int(static_cast<int8_t>(unsigned_diff));
        else return encode_int(static_cast<int8_t>(static_cast<int>(unsigned_diff) - 256));
    } else {
        unsigned_diff = b - a;
        if (unsigned_diff <= 128) return encode_int(static_cast<int8_t>(-static_cast<int>(unsigned_diff)));
        else return encode_int(static_cast<int8_t>(256 - static_cast<int>(unsigned_diff)));
    }
}

int8_t static inline decode_int(uint8_t const& a) {
    return (a & 0b1) ? (-((a - 1) >> 1) - 1) : (a >> 1);
}

uint8_t static inline decoded_sum(uint8_t const& a, uint8_t const& encoded) {
    int8_t const decoded_val = decode_int(encoded);
    if (decoded_val > 0 && decoded_val > (255 - a)) {
        return static_cast<uint8_t>((a + decoded_val) % 256);
    } else if (decoded_val < 0 && -decoded_val > a) {
        return static_cast<uint8_t>((a + decoded_val) + 256);
    } else {
        return static_cast<uint8_t>(a + decoded_val);
    }
}

Mat subtract_modulo(Mat const& a, Mat const& b) {
    Mat c = a.clone();
    c.forEach<Pixel>([&](Pixel& pixel, int const position[]) -> void{
        auto const& other = b.at<Vec3b>(position[0], position[1]);
        pixel.x = encoded_diff(pixel.x, other[0]);
        pixel.y = encoded_diff(pixel.y, other[1]);
        pixel.z = encoded_diff(pixel.z, other[2]);
    });
    return c;
}

bool test(uint8_t const& a, uint8_t const& b) {
    auto const diff = encoded_diff(a, b);
    auto const sum = decoded_sum(b, diff);
    if (sum != a) throw logic_error("decoded sum " + std::to_string(sum) + " does not match original value " + std::to_string(a) + " with second value " + std::to_string(b) + " and intermediary diff " + std::to_string(diff));
    return true;
}

int main() {
    avdevice_register_all(); // for device
    auto const outfile = "tmp.qoi";
    AVFormatContextPtr pAVFormatContext = OpenCam("/dev/video0");

    ofstream output(outfile, ios_base::out|ios_base::binary);
    output.write(reinterpret_cast<char const*>(&framerate), sizeof(framerate));

    uint32_t frames_sent = 0, frames_encoded = 0;
    Mat last;
    while (pAVFormatContext && frames_sent < 100) {
        auto packet = CaptureCam(*pAVFormatContext);
        Mat img(height, width, CV_8UC2, (void *) packet->data), rgb, bgr;
        cvtColor(img, img, COLOR_YUV2RGB_YUYV);
//        if (frames_sent > 0) {
//            rgb = subtract_modulo(img, last);
//        } else {
            rgb = img;
//        }
//        last = img.clone();
        frames_encoded += encode(rgb, output);
//        cvtColor(rgb, bgr, COLOR_RGB2BGR);
//        imshow("cam", bgr);
//        waitKey(5);
        frames_sent++;
    }
    std::cout << frames_sent << " frames sent" << std::endl;
    std::cout << frames_encoded << " frames encoded" << std::endl;
    output.close();

    return 0;
}
