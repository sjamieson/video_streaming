#include <cstdio>
#include <cstdlib>

#define __STDC_CONSTANT_MACROS

#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#define QOI_IMPLEMENTATION
#define QOI_MALLOC(sz) reinterpret_cast<unsigned char*>(malloc(sz))
#define QOI_FREE(sz) free(sz)
#include "qoi/qoi.h"

using namespace std;
using namespace cv;
typedef cv::Point3_<uint8_t> Pixel;

cv::Mat decode(ifstream& in) {
    int len;
    in.read(reinterpret_cast<char*>(&len), sizeof(len));
    if (len <= 0) {
        return {};
    }
    std::vector<char> data(len);
    in.read(data.data(), len);
    qoi_desc desc{0,0,3,QOI_SRGB};
    void* decoded = qoi_decode(data.data(), len, &desc, 3);
    if (decoded == nullptr || desc.width == 0 || desc.height == 0) {
        return {};
    }
    return {static_cast<int>(desc.height), static_cast<int>(desc.width), CV_8UC3, decoded};
}



int8_t static inline decode_int(uint8_t const& a) {
    return (a & 0b1) ? (-((a - 1) >> 1) - 1) : (a >> 1);
}

uint8_t static inline decoded_sum(uint8_t const& a, uint8_t const& encoded) {
    int8_t const decoded_val = decode_int(encoded);
    if (decoded_val > 0 && decoded_val > (255 - a)) {
        return static_cast<uint8_t>((a + decoded_val) % 256);
    } else if (decoded_val < 0 && -decoded_val > a) {
        return static_cast<uint8_t>((a + decoded_val) + 256);
    } else {
        return static_cast<uint8_t>(a + decoded_val);
    }
}

Mat add_modulo(Mat const& a, Mat const& diff) {
    Mat c = a.clone();
    c.forEach<Pixel>([&](Pixel& pixel, int const position[]) -> void{
        auto const& other = diff.at<Vec3b>(position[0], position[1]);
        pixel.x = decoded_sum(pixel.x, other[0]);
        pixel.y = decoded_sum(pixel.y, other[1]);
        pixel.z = decoded_sum(pixel.z, other[2]);
    });
    return c;
}

int main() {
    auto const infile = "tmp.qoi";
    ifstream input(infile, ios_base::in|ios_base::binary);
    uint32_t framerate;
    input.read(reinterpret_cast<char *>(&framerate), sizeof(uint32_t));

    uint32_t frames_read = 0, frames_decoded = 0;
    Mat last;
    while (input.good()) {
        Mat img = decode(input), rgb, bgr;
        frames_read++;
        if (img.empty()) continue;
//        if (frames_decoded > 0) {
//            rgb = add_modulo(last, img);
//        } else {
            rgb = img;
//        }
//        last = rgb.clone();
        cvtColor(rgb, bgr, COLOR_RGB2BGR);
        imshow("cam", bgr);
        waitKey(1000. / framerate);
        frames_decoded++;
    }
    std::cout << frames_read << " frames read" << std::endl;
    std::cout << frames_decoded << " frames decoded" << std::endl;
    input.close();

    return 0;
}
