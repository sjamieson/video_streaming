#include <cstdio>
#include <cstdlib>

#define __STDC_CONSTANT_MACROS

#include <iostream>

extern "C" {
//#include <libavcodec/avcodec.h>
//#include <libavformat/avformat.h>
//#include <libavdevice/avdevice.h>
#include <libavutil/imgutils.h>
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
}
#include <avcodec/avcodec.h>
#include <avcodec/packet.h>
#include <avformat/avformat.h>
#include <avdevice/avdevice.h>
#include <avutil/dict.h>
#include <avutil/frame.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
using namespace avpp;

uint32_t const width = 1280, height = 720, framerate = 10;

AVFormatContextPtr OpenCam(std::string const& device_url) {
    AVDictionaryPtr options = dict_parse_string("framerate=" + std::to_string(framerate)
            + ",video_size=" + std::to_string(width) + "x" + std::to_string(height)
            + ",pixel_format=yuyv422");
    // check video source
    return open_input(device_url, "v4l2", options.get());
} // end function

AVPacketPtr CaptureCam(AVFormatContext &pAVFormatContext) {
    auto packet = packet_alloc();
    auto const ret = av_read_frame(&pAVFormatContext, packet.get());
    if (ret || packet->size == 0) {
        cout << "Failed to capture image" << endl;
    }
    return std::move(packet);
} // end function

size_t encode(AVFrame *frame, AVStream *vstrm, AVCodecContext* codec, AVFormatContext *outctx, SwsContext *swsctx, int64_t &frame_pts,
           cv::Mat const &image) {
    // convert cv::Mat(OpenCV) to AVFrame(FFmpeg)
    const int stride[] = {static_cast<int>(image.step[0])};
    sws_scale(swsctx, &image.data, stride, 0, image.rows, frame->data, frame->linesize);
    frame->pts = frame_pts++;
    // encode video frame
    auto pkt = packet_alloc();
    pkt->data = nullptr;
    pkt->size = 0;
    av_init_packet(pkt.get());
    int ret = avcodec_send_frame(codec, frame);
    if (ret < 0) {
        std::cerr << "fail to avcodec_send_frame: ret=" << ret << "\n" << endl;
        return 0;
    }
    size_t frames_encoded = 0;
    while (ret >= 0) {
        ret = avcodec_receive_packet(codec, pkt.get());
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            std::cerr << "fail to avcodec_receive_packet: ret=" << ret << endl;
            break;
        }
        av_packet_rescale_ts(pkt.get(), codec->time_base, vstrm->time_base);
        pkt->stream_index = vstrm->index;

        /* Write the compressed frame to the media file. */
        ret = av_write_frame(outctx, pkt.get());
        /* pkt is now blank (av_interleaved_write_frame() takes ownership of
         * its contents and resets pkt), so that no unreferencing is necessary.
         * This would be different if one used av_write_frame(). */
        if (ret < 0) {
            fprintf(stderr, "Error while writing output packet\n");
            exit(1);
        }
        frames_encoded++;
    }
    return frames_encoded;
}

size_t drain(AVStream *vstrm, AVCodecContext* codec, AVFormatContext *outctx, int64_t &frame_pts) {
    int ret = avcodec_send_frame(codec, nullptr);
    if (ret < 0) {
        std::cerr << "fail to enter draining mode! ret=" << ret << "\n" << endl;
        return ret;
    }
    auto pkt = packet_alloc();
    pkt->data = nullptr;
    pkt->size = 0;
    av_init_packet(pkt.get());
    size_t frames_encoded = 0;
    while (true) {
        ret = avcodec_receive_packet(codec, pkt.get());
        if (ret == AVERROR_EOF) {
            break;
        } else if (ret < 0) {
            if (ret == AVERROR(EAGAIN)) {
                std::cerr << "fail to enter draining mode! ret=" << ret << "\n" << endl;
            } else {
                std::cerr << "fail to avcodec_receive_packet: ret=" << ret << endl;
            }
            break;
        }
        av_packet_rescale_ts(pkt.get(), codec->time_base, vstrm->time_base);
        pkt->stream_index = vstrm->index;

        /* Write the compressed frame to the media file. */
        ret = av_write_frame(outctx, pkt.get());
        /* pkt is now blank (av_interleaved_write_frame() takes ownership of
         * its contents and resets pkt), so that no unreferencing is necessary.
         * This would be different if one used av_write_frame(). */
        if (ret < 0) {
            fprintf(stderr, "Error while writing output packet\n");
            exit(1);
        }
        frames_encoded++;
    }
    return frames_encoded;
}

int main() {
    avdevice_register_all(); // for device
    auto const outfile = "tmphq-2.mp4";
    AVFormatContextPtr pAVFormatContext = OpenCam("/dev/video0");

    // open output format context
    AVFormatContext *outctx = nullptr;
    int ret = avformat_alloc_output_context2(&outctx, nullptr, nullptr, outfile);
    if (ret < 0) {
        std::cerr << "fail to avformat_alloc_output_context2(" << outfile << "): ret=" << ret;
        return 2;
    }

    // open output IO context
    ret = avio_open2(&outctx->pb, outfile, AVIO_FLAG_WRITE, nullptr, nullptr);
    if (ret < 0) {
        std::cerr << "fail to avio_open2: ret=" << ret;
        return 2;
    }

    AVRational fps = {framerate, 1};

    // create new video stream
    AVCodec *vcodec = avcodec_find_encoder(outctx->oformat->video_codec);
    AVStream *vstrm = avformat_new_stream(outctx, vcodec);
    if (!vstrm) {
        std::cerr << "fail to avformat_new_stream";
        return 2;
    }

    AVCodecContext* codec_ctx = avcodec_alloc_context3(vcodec);
    if (codec_ctx == nullptr) {
        std::cerr << "Failed to allocate codec context!" << std::endl;
    }
    vstrm->codecpar->width = width;
    vstrm->codecpar->bit_rate = 400000;
    vstrm->codecpar->height = height;
    vstrm->codecpar->format = vcodec->pix_fmts[0];
    vstrm->codecpar->codec_id = AV_CODEC_ID_H264;
    vstrm->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
    if ((ret = avcodec_parameters_to_context(codec_ctx, vstrm->codecpar)) < 0) {
        std::cerr << "Failed to copy decoder parameters to input decoder context: ret=" << ret << std::endl;
    }
    codec_ctx->time_base = vstrm->time_base = av_inv_q(fps);
    vstrm->r_frame_rate = vstrm->avg_frame_rate = fps;
    codec_ctx->pix_fmt = vcodec->pix_fmts[0];
    if (outctx->oformat->flags & AVFMT_GLOBALHEADER)
        codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    // open video encoder
    AVDictionaryPtr options = dict_parse_string("preset=superfast,qp=0");
    ret = codec_open2(*codec_ctx, *vcodec, options);
    if (ret < 0) {
        std::cerr << "fail to avcodec_open2: ret=" << ret << std::endl;
        return 2;
    }

    std::cout
            << "outfile: " << outfile << "\n"
            << "format:  " << outctx->oformat->name << "\n"
            << "vcodec:  " << vcodec->name << "\n"
            << "size:    " << width << 'x' << height << "\n"
            << "fps:     " << av_q2d(fps) << "\n"
            << "pixfmt:  " << av_get_pix_fmt_name(codec_ctx->pix_fmt) << "\n"
            << std::flush;

    // initialize sample scaler
    SwsContext *swsctx = sws_getCachedContext(
            nullptr, width, height, AV_PIX_FMT_BGR24,
            width, height, codec_ctx->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);
    if (!swsctx) {
        std::cerr << "fail to sws_getCachedContext";
        return 2;
    }

    // allocate frame buffer for encoding
    AVFramePtr frame = frame_alloc();
    std::vector<uint8_t> framebuf(av_image_get_buffer_size(codec_ctx->pix_fmt, width, height, 1));
    av_image_fill_arrays(frame->data, frame->linesize, framebuf.data(), codec_ctx->pix_fmt, width, height, 1);
    frame->width = width;
    frame->height = height;
    frame->format = static_cast<int>(codec_ctx->pix_fmt);
    if (av_frame_get_buffer(frame.get(), 0) < 0) {
        std::cerr << "failed to allocate frame data" << endl;
        return 2;
    }

    int64_t frame_pts = 0;
    size_t frames_sent = 0, frames_encoded = 0;
    avcodec_parameters_from_context(vstrm->codecpar, codec_ctx);
    ret = avformat_write_header(outctx, nullptr);
    if (ret < 0) {
        std::cerr << "failed to wriite header" << endl;
        return 2;
    }
    while (pAVFormatContext && frames_sent < 100) {
        frames_sent++;
        auto packet = CaptureCam(*pAVFormatContext);
        Mat img(height, width, CV_8UC2, (void *) packet->data), bgr;
        cvtColor(img, bgr, COLOR_YUV2BGR_YUYV);
        frames_encoded += encode(frame.get(), vstrm, codec_ctx, outctx, swsctx, frame_pts, bgr);
        imshow("cam", bgr);
        waitKey(5);
    }
    std::cout << frames_sent << " frames sent" << std::endl;
    std::cout << frames_encoded << " frames encoded, draining buffer" << std::endl;
    frames_encoded += drain(vstrm, codec_ctx, outctx, frame_pts);
    avcodec_flush_buffers(codec_ctx);
    av_write_trailer(outctx);
    std::cout << frames_encoded << " frames encoded" << std::endl;

    avio_closep(&outctx->pb);
    avcodec_parameters_free(&vstrm->codecpar);
    avcodec_free_context(&codec_ctx);
    sws_freeContext(swsctx);
    avformat_free_context(outctx);
    return 0;
}
